﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using static Player_Manager;

public class ChargeMeter : MonoBehaviour
{
    
    
    [SerializeField]
    Slider m_sliderPink;
    [SerializeField]
    Slider m_sliderBlue;

    [SerializeField]
    float m_middleTop;
    [SerializeField]
    Vector3 m_middleWidth;
    Player m_activePlayer;
    RectTransform[] m_savedRect = new RectTransform[2];
    void Start()
    {
        m_savedRect[0] = m_sliderPink.gameObject.GetComponent<RectTransform>();
        m_savedRect[1] = m_sliderBlue.gameObject.GetComponent<RectTransform>();
    }
    void Update()
    {
        m_activePlayer = FindObjectOfType<Player_Manager>().ActivePlayer;

        switch(m_activePlayer)
        {
            case Player.NONE: 
            m_sliderPink.value = 0;
            m_sliderBlue.value = 0;
            break;
            case Player.TWO:
            m_sliderPink.value = FindObjectOfType<Player_Manager>().Charge;
            m_sliderBlue.value = 0;
            m_sliderBlue.gameObject.GetComponent<RectTransform>().position = m_savedRect[1].position;
            m_sliderBlue.gameObject.GetComponent<RectTransform>().localScale = m_savedRect[1].localScale;
                if(m_sliderPink.value == 100)
                { 
                  
                }
            break;
            case Player.ONE:
            m_sliderBlue.value = FindObjectOfType<Player_Manager>().Charge;
            m_sliderPink.value = 0;
            m_sliderPink.gameObject.GetComponent<RectTransform>().position = m_savedRect[0].position;
            m_sliderPink.gameObject.GetComponent<RectTransform>().localScale = m_savedRect[0].localScale;
                if(m_sliderBlue.value == 100)
                { 
                   
                }
            break;
        }
        
    }
}
