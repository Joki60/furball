﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Variables;
using static Player_Manager;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    [SerializeField]
    private Variables m_variables;
    private Score_Manager m_score;
    private float m_displacementTimer = 0;
    private float m_displacementTime = 4;
    private bool m_hasTriggered = false;
    void Start()
    {
        Time.timeScale = 1;
        m_score = FindObjectOfType<Score_Manager>();

    }

    // Update is called once per frame
    void Update()
    {
        if(!m_hasTriggered){return;}
        m_displacementTimer += Time.deltaTime / Time.timeScale;
        if(m_displacementTimer > m_displacementTime){SwitchScene();}
        
    }
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.tag == "Cat")
        {
            FindObjectOfType<Prompt_Manager>().Display(Prompt_Manager.Prompts.GOAL,1);
            m_hasTriggered = true;
            Time.timeScale = 0.2f;
        }
    }
    private void SwitchScene()
    {
         m_variables.one_score_SET(m_score.GetScore(Player.ONE),m_variables.Round-1);

            m_variables.two_score_SET(m_score.GetScore(Player.TWO),m_variables.Round-1);

            switch(m_variables.Round)
            {
                case 1:
                    m_variables.Round = 2;
                break;
                case 2:
                    m_variables.Round = 3;
                break;
                case 3:
                    m_variables.Round = 0;
                break;
            }
            SceneManager.LoadScene(m_variables.Round);
    }
}
