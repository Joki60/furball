﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
class PickUp_Object
{
    public GameObject m_object;
    public int m_weight;
}

public class PickUp_Spawn : MonoBehaviour
{
    //The spacing is only for the x value
    static float m_spacing = 30;
    [SerializeField]
    PickUp_Object[] m_pickUps;
    [SerializeField]
    [Tooltip("The anchors tells the script between which points to space the pick ups. This is carried out sequentially")]
    Vector2[] m_anchors;

    // Start is called before the first frame update
    void Start()
    {
        if(m_anchors.Length < 2 || m_pickUps.Length < 1){return;} 
        Vector3[] t_positions = GetObjectPositions();
        foreach (Vector3 position in t_positions)
        {
            Instantiate(RandomObject(),position,Quaternion.identity);
        }
    }
    private GameObject RandomObject()
    {
        if(m_pickUps.Length == 1){return m_pickUps[0].m_object;}
        int t_totalWeight = 0;
        foreach (PickUp_Object item in m_pickUps)
        {
            t_totalWeight += item.m_weight;
        }
        int t_rand = Random.Range(0,t_totalWeight);
        int t_currentWeight = 0;
        foreach (PickUp_Object item in m_pickUps)
        {
            t_currentWeight += item.m_weight;
            if(t_rand <= t_currentWeight)
            {
                return item.m_object;
            } 
        }
        return null;
    }
    private Vector3[] GetObjectPositions()
    {
        List<Vector3> t_positions = new List<Vector3>();
        for (int i = 0; i < m_anchors.Length -1; i++)
        {
            int t_fishAmount = Mathf.Abs((int)(Vector2.Distance(m_anchors[i+1],m_anchors[i])/m_spacing));
            for(int o = 0; o < t_fishAmount; o ++)
            {
                float x = (m_anchors[i +1].x - m_anchors[i].x)/(float)t_fishAmount;
                float y = (m_anchors[i +1].y - m_anchors[i].y)/(float)t_fishAmount;
                t_positions.Add(new Vector2(transform.position.x,transform.position.y) + m_anchors[i] + new Vector2(x * o, y*  o));
            }
        }  
        return t_positions.ToArray();
    }
    private void OnDrawGizmosSelected() 
    {
      if(m_anchors.Length < 2){return;}
      foreach (Vector2 position in m_anchors)
      {
          Gizmos.color = Color.red;
          Gizmos.DrawSphere(this.transform.position + (Vector3)position, 10);
      }
      Vector3[] t_positions = GetObjectPositions();
//      print(t_positions.Length);
      foreach (Vector3 position in t_positions)
      {
        Gizmos.color = Color.green;
        //print(position);
        Gizmos.DrawCube(position,new Vector2(10,10));   
      }   
    }
}
