﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Player_Manager;

[RequireComponent(typeof(Collider2D))]
public class IPickUp : MonoBehaviour
{
    [SerializeField]
    AudioClip m_pickUpAudio;
    [SerializeField]
    int m_points;
    SoundManager m_soundManager;
    Score_Manager m_scoreManager;
    #region hasBeenPickedUp
    protected bool m_hasBeenPickedUp = false;
    [Header("When picked up")]
    [SerializeField]
    float m_transitionTime;
    float m_transitionTimer;
    [SerializeField]
    EasingFunction.Ease m_transitionEase_X;
    [SerializeField]
    EasingFunction.Ease m_transitionEase_Y;
    [SerializeField]
    EasingFunction.Function m_transitionFunc_X;
    EasingFunction.Function m_transitionFunc_Y;
    Vector3 m_startPosition;
    Player m_targetPlayer;
    [SerializeField]
    Vector3 m_targetSize;
    protected Vector3 m_savedSize;
    [SerializeField]
    float m_shrinkTime;
    float m_shrinkTimer;
    [SerializeField]
    EasingFunction.Ease m_shrinkEase;
    EasingFunction.Function m_shrinkFunction;


    #endregion
    void Awake()
    {

        m_soundManager = GameObject.FindObjectOfType<SoundManager>();
        m_scoreManager = GameObject.FindObjectOfType<Score_Manager>();
        GetComponent<Collider2D>().isTrigger = true;
        m_transitionFunc_X = EasingFunction.GetEasingFunction(m_transitionEase_X);
        m_transitionFunc_Y = EasingFunction.GetEasingFunction(m_transitionEase_Y);
        m_shrinkFunction = EasingFunction.GetEasingFunction(m_shrinkEase);
    }

    // Update is called once per frame
    public virtual void Update()
    {
        TransitionLogic();
    }
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.tag == "Cat" && GameObject.FindObjectOfType<Player_Manager>().ActivePlayer != Player_Manager.Player.NONE)
        {
            if(m_pickUpAudio != null)m_soundManager.PlaySingle(m_pickUpAudio);
            m_targetPlayer = GameObject.FindObjectOfType<Player_Manager>().ActivePlayer;
           // m_scoreManager.Add(GameObject.FindObjectOfType<Player_Manager>().ActivePlayer,m_points);
            #region Transition Logic
            m_hasBeenPickedUp = true;
            this.GetComponent<Collider2D>().enabled = false;
            m_startPosition = this.transform.position;
            m_savedSize = this.transform.localScale;
            #endregion
            print("PICK UP");
        }   
    }
    protected void TransitionLogic()
    {
        if(m_hasBeenPickedUp)
        {
            ModulateTo();
            m_transitionTimer += Time.deltaTime;
            m_transitionTimer = (m_transitionTimer > m_transitionTime)? m_transitionTime: m_transitionTimer;
            Vector3 m_targetPosition = Vector3.zero;
            switch(m_targetPlayer)
            {
                case Player.ONE:
                    m_targetPosition = GameObject.FindGameObjectWithTag("StepPink").GetComponent<RectTransform>().transform.position;
                break;
                case Player.TWO:
                     m_targetPosition = GameObject.FindGameObjectWithTag("StepBlue").GetComponent<RectTransform>().transform.position;
                break;
            }
            m_targetPosition.z = 0;
            m_targetPosition = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraBehaviour>().GetWorldPositionOnPlane(m_targetPosition,0);
          

            transform.position = new Vector3(
                                             m_transitionFunc_X(m_startPosition.x,m_targetPosition.x,m_transitionTimer/m_transitionTime),
                                             m_transitionFunc_Y(m_startPosition.y,m_targetPosition.y,m_transitionTimer/m_transitionTime),
                                             m_startPosition.z);
            
            if(m_transitionTimer >= m_transitionTime)
            {
                m_scoreManager.Add(GameObject.FindObjectOfType<Player_Manager>().ActivePlayer,m_points);
                Destroy(this.gameObject);
            }
        }

    }
    protected void ModulateTo()
    {
        if(m_shrinkTimer < m_shrinkTime){

        m_shrinkTimer += Time.deltaTime;
        m_shrinkTimer = (m_shrinkTimer > m_shrinkTime)? m_shrinkTime: m_shrinkTimer;
        transform.localScale = new Vector3(
                                            m_shrinkFunction(m_savedSize.x,m_targetSize.x, m_shrinkTimer/m_shrinkTime), 
                                            m_shrinkFunction(m_savedSize.y,m_targetSize.y, m_shrinkTimer/m_shrinkTime),transform.localScale.z);
        }

    }
    
}
