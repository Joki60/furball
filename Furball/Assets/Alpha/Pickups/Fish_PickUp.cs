﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish_PickUp : IPickUp
{
    [SerializeField]
    float m_bobbingAmount;
    [SerializeField]
    float m_bobbingTime;
    float m_bobbingTimer = 0;
    bool m_direction = true;
    [SerializeField]
    EasingFunction.Ease m_bobbingEasing;
    EasingFunction.Function m_bobbingFunction;
    Vector3 m_anchorPoint;
    [SerializeField]
    bool m_randomBob;
    void Start()
    {
        m_bobbingFunction = EasingFunction.GetEasingFunction(m_bobbingEasing);
        m_anchorPoint = this.transform.position;
        if(m_randomBob){m_bobbingTimer = Random.Range(0,m_bobbingTime);}
        
    }
    public override void Update()
    {
        if(!m_hasBeenPickedUp)
        {
            m_bobbingTimer += (m_direction)? Time.deltaTime: -Time.deltaTime;
            m_bobbingTimer = (m_bobbingTimer > m_bobbingTime)? m_bobbingTime: (m_bobbingTimer < 0)? 0: m_bobbingTimer;
            if(m_bobbingTimer >= m_bobbingTime){m_direction = false;}
            if(m_bobbingTimer <= 0){m_direction = true;}

            transform.position = new Vector3(m_anchorPoint.x,m_bobbingFunction(m_anchorPoint.y - m_bobbingAmount, m_anchorPoint.y + m_bobbingAmount,m_bobbingTimer/m_bobbingTime));
        }
        TransitionLogic();
    }   
}
