﻿using System.Collections;
using System.Collections.Generic;
using RavingBots.MultiInput;
using UnityEngine;
using UnityEngine.Rendering;
using static Player_Manager;
using static EasingFunction;

[RequireComponent(typeof(Collider2D))]
public class Player_Spirit : MonoBehaviour
{
    private static float m_hoveringRadius = 30.0f;
    private static Vector2 m_hoverOffset = new Vector2(0, 20);
    /*[SerializeField]
    string m_movementAxisX;
    [SerializeField]
    string m_movementAxisY;
    [SerializeField]
    float m_interactibleRadius;
    [SerializeField]
    LayerMask m_objectMask;
    [SerializeField] 
    float m_forceMultiplierMovement;
    [SerializeField]
    float m_forceMultiplierLaunch;
    Rigidbody2D m_rb;

    public IDevice m_mouse;*/
    [SerializeField]
    private Player m_player;
    [Header("Controlls")]
    [SerializeField]
    KeyCode m_leftKey;
    [SerializeField]
    KeyCode m_rightKey;

    public Player Player{get{return m_player;}}
    public bool InTransition
    {
        get
        {
            switch(m_currentState)
            {
                case PlayerStates.TRANSITIONING: return true;
                case PlayerStates.DEATTACHED: return true;
                default: return false;
            }
        }
    }
    public KeyCode LeftKey{get{return m_leftKey;}}
    public KeyCode RightKey{get{return m_rightKey;}}

    public enum PlayerStates
    {
        MOVING,
        TRANSITIONING,
        ATTACHED,
        DEATTACHED,
        DAZED
    }
    [HideInInspector]
    public PlayerStates m_currentState = PlayerStates.MOVING;
    private GameObject m_cat;
    private Player_Manager m_playerManager;

    #region FOR WHEN THE PLAYER SNAPS TO THE CAT
    [HideInInspector]
    public Vector3 m_catJointToSnapTo;
    Vector3 m_savedPosition = Vector3.zero;
    float m_snapTimer = 0;
    [Header("Movement")]
    [SerializeField]
    float m_snapTime = 1;
    [SerializeField]
    EasingFunction.Ease m_snapEasing_X;
    [SerializeField]
    EasingFunction.Ease m_snapEasing_Y;
    EasingFunction.Function  m_snapFunction_X;
    EasingFunction.Function  m_snapFunction_Y;
    [SerializeField]
    [Tooltip("In seconds")]
    float m_launchSpeed;
    float m_launchTimer;
    EasingFunction.Ease m_launchEasing_X = EasingFunction.Ease.EaseOutBack;
    EasingFunction.Ease m_launchEasing_Y = EasingFunction.Ease.EaseOutBack;
    EasingFunction.Function m_launchFunction_X;
    EasingFunction.Function m_launchFunction_Y;    
    #endregion
    /*bool m_hasJumped;
    [SerializeField]
    LayerMask m_catMask;*/
    #region FOR WHEN THE CATS ARE HOVERING
    [SerializeField]
    EasingFunction.Ease m_hoverEasing_X;
    [SerializeField]
    EasingFunction.Ease m_hoverEasing_Y;
    EasingFunction.Function m_hoverFunction_X;
    EasingFunction.Function m_hoverFunction_Y;
    float m_hoveringWidthTimer;
    float m_hoveringTimer;
    //The timer for making the cats hover in a circle
    [SerializeField]
    float m_hoveringTime;
    [SerializeField]
    [Range(0,359)]
    [Tooltip("In degrees")]
    float m_hoveringAngle;
    [SerializeField]
    [Tooltip("Discribes how the spirits travel in a spherical fashion. 0 = Straight line, 1 = circle")]
    [Range(0,1)]
    float m_hoveringWidth;
    [SerializeField]
    bool m_invertX;
    [SerializeField]
    bool m_invertY;
    [SerializeField]
    bool m_hoverForward = true;
    bool m_hoverWidthForward = true;
    Vector3 m_orderInLayer = new Vector3(0,2);
    #region HoveringVectors
    Vector3 m_hoverStart;
    Vector3 m_hoverEnd;
    #endregion
    #endregion

    
    void Start()
    {
        m_playerManager = GameObject.FindObjectOfType<Player_Manager>();
        m_cat = GameObject.FindGameObjectWithTag("Cat");
        //m_rb = this.GetComponent<Rigidbody2D>();
        m_snapFunction_X = GetEasingFunction(m_snapEasing_X);
        m_snapFunction_Y = GetEasingFunction(m_snapEasing_Y);
        m_hoverFunction_X = GetEasingFunction(m_hoverEasing_X);
        m_hoverFunction_Y = GetEasingFunction(m_hoverEasing_Y);
        m_launchFunction_X = GetEasingFunction(m_launchEasing_X);
        m_launchFunction_Y = GetEasingFunction(m_launchEasing_Y);
       // m_hoveringWidthTimer = m_hoveringTime/ 2;
       m_hoveringTimer = (m_hoverForward)? m_hoveringTime: 0;
    }
    void Update()
    {
        // if(Input.GetKeyDown(m_leftKey)){print("Left");}
        // if(Input.GetKeyDown(m_rightKey)){print("Right");}
        switch(m_currentState)
        {
            case PlayerStates.MOVING:

            if(GetComponent<SortingGroup>().sortingOrder != m_orderInLayer.y && m_hoverForward)
            {GetComponent<SortingGroup>().sortingOrder = (int)m_orderInLayer.y;}
            else if(GetComponent<SortingGroup>().sortingOrder != m_orderInLayer.x && !m_hoverForward)
            {GetComponent<SortingGroup>().sortingOrder = (int)m_orderInLayer.x;}

            float deltaTime = Time.deltaTime;
            m_hoverForward = (m_hoveringTimer < 0 && !m_hoverForward)? true: (m_hoveringTimer > m_hoveringTime && m_hoverForward)? false: m_hoverForward;
            m_hoveringTimer = 
            (m_hoverForward && m_hoveringTimer <= m_hoveringTime)? m_hoveringTimer + deltaTime:
            (!m_hoverForward && m_hoveringTimer >= 0)? m_hoveringTimer - deltaTime:
            (m_hoveringTimer > m_hoveringTime)? m_hoveringTime:
            (m_hoveringTimer < 0)? 0:
             m_hoveringTimer; //<- The last part will never happen but it's for safety;

            #region Setting the hover vectors
            Vector2 cord = GetPointOnUnitCircle(m_hoveringAngle);

            m_hoverStart = 
            (m_invertX && !m_invertY)? new Vector3(cord.x * -1,cord.y) * m_hoveringRadius:
            (m_invertY && !m_invertX)? new Vector3(cord.x,-1 * cord.y) * m_hoveringRadius:
            (m_invertY &&  m_invertY)? new Vector3(cord.x   ,  cord.y) * m_hoveringRadius * -1:
                                       new Vector3(cord.x   ,  cord.y) * m_hoveringRadius;
        
            m_hoverEnd = m_hoverStart * -1;                        
            #endregion 

            Vector3 hoverPosition = new Vector3(
                m_hoverFunction_X(m_hoverStart.x,m_hoverEnd.x,m_hoveringTimer/m_hoveringTime),
                m_hoverFunction_Y(m_hoverStart.y,m_hoverEnd.y,m_hoveringTimer/m_hoveringTime));
            
            transform.position = m_cat.transform.position + hoverPosition + (Vector3)m_hoverOffset;
            m_hoveringAngle = (m_hoveringAngle > 359)? 0:(m_hoveringAngle < 0)? 359: m_hoveringAngle += Time.deltaTime * 20/m_hoveringTime;

           /* if((Input.GetKeyUp(m_leftKey) && Input.GetKeyUp(m_rightKey)) && !m_hasJumped)
            {
                m_hasJumped = true;
            }
            if((Input.GetKeyDown(m_leftKey) && Input.GetKeyDown(m_rightKey)) && m_hasJumped)
            {
                OnInteract();
            }*/
            break;
            //This only works if the cat isn't moving at all. It has to be stationary when switching states.
            case PlayerStates.TRANSITIONING:
            m_catJointToSnapTo = (m_player == Player.ONE)? m_cat.GetComponent<Cat>().m_anchor_PlayerOne.position: m_cat.GetComponent<Cat>().m_anchor_PlayerTwo.position;
            if(m_savedPosition == Vector3.zero)
            {
                m_savedPosition = transform.position;
            }
            if(m_snapTimer < m_snapTime)
            {
                // Snaptimer equals 1 if larger than 1, 0 if less than zero, otherwise we add time (Basically how long it took for the last frame to update);
                m_snapTimer = (m_snapTimer > m_snapTime)? m_snapTime:(m_snapTimer < 0)? 0: m_snapTimer + Time.deltaTime;
                transform.position = new Vector3(m_snapFunction_X(m_savedPosition.x,m_catJointToSnapTo.x,m_snapTimer/m_snapTime),m_snapFunction_Y(m_savedPosition.y,m_catJointToSnapTo.y,m_snapTimer/m_snapTime),m_catJointToSnapTo.z);
                break;
            }
            m_snapTimer = 0;
            m_savedPosition = Vector3.zero;
            m_currentState = PlayerStates.ATTACHED;
            m_cat.GetComponent<Cat>().SwitchActiveWalkControlls(m_leftKey,m_rightKey);
            m_playerManager.UpdateActivePlayer(m_player);
            break;

            case PlayerStates.DEATTACHED:
            //Start position, might shift as the cat might be falling, but that's no problem
            m_catJointToSnapTo = (m_player == Player.ONE)? m_cat.GetComponent<Cat>().m_anchor_PlayerOne.position: m_cat.GetComponent<Cat>().m_anchor_PlayerTwo.position;
            //Setting the final poisiton
            Vector3 t_targetPosition = (Vector3)(GetPointOnUnitCircle(m_hoveringAngle));

              t_targetPosition = 
            (m_invertX && !m_invertY)? new Vector3(t_targetPosition.x * -1,t_targetPosition.y) * m_hoveringRadius:
            (m_invertY && !m_invertX)? new Vector3(t_targetPosition.x,-1 * t_targetPosition.y) * m_hoveringRadius:
            (m_invertY &&  m_invertY)? new Vector3(t_targetPosition.x   ,  t_targetPosition.y) * m_hoveringRadius * -1:
                                       new Vector3(t_targetPosition.x   ,  t_targetPosition.y) * m_hoveringRadius;

            t_targetPosition +=  m_cat.transform.position + (Vector3)m_hoverOffset;

            m_launchTimer += Time.deltaTime;if(m_launchTimer > m_launchSpeed){m_launchTimer = m_launchSpeed;}
            Vector3 t_transform = new Vector3(
            m_launchFunction_X(m_catJointToSnapTo.x,t_targetPosition.x,m_launchTimer/m_launchSpeed),
            m_launchFunction_Y(m_catJointToSnapTo.y,t_targetPosition.y,m_launchTimer/m_launchSpeed));
            transform.position = t_transform;

            if(m_launchTimer >= m_launchSpeed)
            {
                m_hoveringTimer = 0;
                m_launchTimer = 0;
                m_currentState = PlayerStates.MOVING;
            }
            break;
            case PlayerStates.ATTACHED:
            if(GetComponent<SortingGroup>().sortingOrder != m_orderInLayer.x){GetComponent<SortingGroup>().sortingOrder = (int)m_orderInLayer.x;}
            switch(m_player)
            {
                case Player.ONE:
                transform.position = GameObject.FindGameObjectWithTag("Cat").GetComponent<Cat>().m_anchor_PlayerOne.position;
                break;
                case Player.TWO:
                transform.position = GameObject.FindGameObjectWithTag("Cat").GetComponent<Cat>().m_anchor_PlayerOne.position;
                break;
            }
            break;
            default:break;

        }
    }
   /* private float GetValue(params InputCode[] codes) {
        foreach (var code in codes) {

            var axis = m_mouse[code];

            if (axis != null && axis.IsHeld)
            {
                return axis.Value;
            }
        }
        return 0;
    }*/
    public void SnapTo(Vector3 p_position)
    {
        m_currentState = PlayerStates.TRANSITIONING;
        m_savedPosition = Vector3.zero;
    }
    public Vector2 GetPointOnUnitCircle(float p_degrees)
    {
        return new Vector2(Mathf.Cos(Mathf.Deg2Rad * p_degrees),Mathf.Sin(Mathf.Deg2Rad * p_degrees));
    }
    public void Launch()
    {
        m_hoveringAngle = Random.Range(0,359);
        m_currentState = PlayerStates.DEATTACHED;
        m_hoverForward = false;
    }
    /*public void OnInteract()
    {
        print("COLLIIIIIDE");
        Collider2D[] objects = Physics2D.OverlapCircleAll(this.transform.position,m_interactibleRadius,m_objectMask);
        foreach (Collider2D collider in objects)
        {
            if(collider.gameObject.GetComponent<InteractibleObject>() != null)
            {
                collider.gameObject.GetComponent<InteractibleObject>().Activate();
            }
        }
    }
    private void OnDrawGizmosSelected() 
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,m_interactibleRadius);
    }*/
    /* 
    public bool CanInteract(GameObject m_gameObject)
    {
        Collider2D cat = Physics2D.OverlapCircle(this.transform.position,m_interactibleRadius,m_catMask);
        //print(Mathf.Abs(Vector3.Distance(this.transform.position, m_gameObject.transform.position)));
        //if(Mathf.Abs(Vector3.Distance(this.transform.position, m_gameObject.transform.position)) < m_interactibleRadius){return true;}return false;
        return (cat);
}*/
}
