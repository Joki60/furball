﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Player_Manager;

[CreateAssetMenu(fileName = "Data", menuName ="Data", order = 1)]
public class Variables : ScriptableObject
{
    private static int roundAmount = 3;
    private int[] one_score = new int[roundAmount];
    private int[] two_score = new int[roundAmount];
    private Player startPlayerLastTurn = Player.NONE;
    private int currentRound = 1;

    public void Reset()
    {
        currentRound = 1;
        startPlayerLastTurn = Player.NONE;
        one_score = new int[roundAmount];
        two_score = new int[roundAmount];
    }
    void Awake()
    {
       currentRound = 1; 
    }
    public int Round
    {
        get{return currentRound;}
        set{currentRound = value;}
    }
    public Player StartPlayerLastTurn
    {
        get{return startPlayerLastTurn;}
        set{startPlayerLastTurn = value;}
    }
    public void one_score_SET(int score,int index)
    {
        if(index > one_score.Length - 1 || index < 0){return;}
        one_score[index] = score; 
    } 
    public int one_score_GET(int index)
    {
        if(index > one_score.Length - 1 || index < 0){return 0;}
        return one_score[index];
    }
    public int one_score_GET_TOTAL()
    {
        int temp = 0;
        for (int i = 0; i < one_score.Length; i++)
        {
          temp += one_score[i];   
        }
        return temp;
    }
    public void two_score_SET(int score,int index)
    {
        if(index > two_score.Length - 1 || index < 0){return;}
        two_score[index] = score; 
    } 
    public int two_score_GET(int index)
    {
        if(index > two_score.Length - 1 || index < 0){return 0;}
        return two_score[index];
    }
    public int two_score_GET_TOTAL()
    {
        int temp = 0;
        for (int i = 0; i < two_score.Length; i++)
        {
          temp += two_score[i];   
        }
        return temp;
    }
}
