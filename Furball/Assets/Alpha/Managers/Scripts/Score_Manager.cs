﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using static Player_Manager;
public class Score_Manager : MonoBehaviour
{
    int m_playerTwoScore;
    int m_playerOneScore;


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       /* print("Player One:" + m_playerOneScore);
        print("Player Two:" + m_playerTwoScore);*/
    }
    public void Add(Player p_player,int p_points)
    {
        switch(p_player)
        {
            case Player.ONE:
            m_playerOneScore += p_points; 
            break;
            case Player.TWO: 
            m_playerTwoScore += p_points;
            break;
            default:break;
        }
    }
    public int GetScore(Player m_player)
    {
        switch(m_player)
        {
            case Player.ONE:
                return m_playerOneScore;
            case Player.TWO:
                return m_playerTwoScore;
            default:
            return 0;
        }
    }
}
