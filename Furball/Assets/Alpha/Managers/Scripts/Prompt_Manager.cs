﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Prompt_Manager : MonoBehaviour
{
    public enum Prompts
    {
        READY,
        SET,
        GO,
        GOAL,
        JUMP_NONE,
        JUMP_ONE,
        JUMP_TWO,
        PLAYER_ONE,
        PLAYER_TWO,
        PRESSBUTTON_ONE,
        PRESSBUTTON_TWO,
        ROUND_ONE,
        ROUND_TWO,
        ROUND_FINAL,
        RUN_NONE,
        RUN_ONE,
        RUN_TWO,
        SPINTHEBALL_NONE,
        SPINTHEBALL_ONE,
        SPINTHEBALL_TWO,
        SWITCH_NONE,
        SWITCH_ONE,
        SWITCH_TWO,
        WIN_EVERONE,
        WIN_ONE,
        WIN_TWO,
        RING_ONE,
        RING_TWO,
        HIGHSCORE_ONE,
        HIGHSCORE_TWO,
        READY_ONE,
        READY_TWO

    }
    [SerializeField]
    Prompts m_removeThisThisIsJustToMatch;

    [SerializeField]
    Image[] m_prompts;

    Dictionary<Prompts,float> m_timers = new Dictionary<Prompts, float>();
    Dictionary<Prompts,float> m_times = new Dictionary<Prompts, float>();
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        /*if(Input.GetKeyDown(KeyCode.Y)){Display(Prompts.READY,0.5f);}
        if(Input.GetKeyDown(KeyCode.U)){Display(Prompts.SET,0.5f);}
        if(Input.GetKeyDown(KeyCode.I)){Display(Prompts.GO,0.2f);}
        if(Input.GetKeyDown(KeyCode.O)){Display(Prompts.PRESSBUTTON_ONE,1);}
        if(Input.GetKeyDown(KeyCode.P)){Display(Prompts.PRESSBUTTON_TWO,1);}*/
        if(m_timers.Count == 0){return;}
        var keys = new List<Prompts>(m_timers.Keys);
        foreach(Prompts key in keys)
        {
            m_timers[key] = m_timers[key] + Time.deltaTime;  
            if(m_timers[key] >= m_times[key])
            {
                m_prompts[(int)key].GetComponent<Animator>().SetBool("isActive",false);
                m_timers.Remove(key);
                m_times.Remove(key);
            }
        }
        
    }
    public void Display(Prompts p_prompt, float m_time)
    {
        if(m_timers.ContainsKey(p_prompt))
        {
            m_timers[p_prompt] = 0;
            m_times[p_prompt] = m_time;
            return;
        }
        m_timers.Add(p_prompt,0);
        m_times.Add(p_prompt,m_time);
        m_prompts[(int)p_prompt].GetComponent<Animator>().SetBool("isActive",true);
    }
}
