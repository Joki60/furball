﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using static BarArea;

public class GameState_Manger : MonoBehaviour
{
    public enum GameStates
    {
        NORMAL = 0,
        FIGHT
    }
    public enum Player
    {
        NONE = 0,
        ONE,
        TWO
    }
    [Header("Player Specific")]
    [SerializeField]
    [Range(0,1)]
    float m_DistractionReduction;
    [SerializeField]
    [Range(0,1)]
    float m_DistractionReset;
    const KeyCode m_tailKeyOne = KeyCode.Joystick1Button0;
    const KeyCode m_tailKeyTwo = KeyCode.K;
    const KeyCode m_tailKeyBoth = KeyCode.B;
    Player m_activePlayer = Player.NONE; 
    GameStates m_currentGameState;
    private DistractionMeter_Pendulum m_pendulum;

    public GameStates CurrentSate{get{return m_currentGameState;}}
    public Player ActivePlayer{get{return m_activePlayer;}}

    private GameObject m_playerOne;
    private GameObject m_playerTwo;
    private GameObject m_cat;
    void Start()
    {
        m_playerOne = GameObject.FindGameObjectWithTag("Player1");
        m_playerTwo = GameObject.FindGameObjectWithTag("Player2");
        m_cat = GameObject.FindGameObjectWithTag("Cat");
        m_pendulum = GameObject.FindGameObjectWithTag("Pendulum").GetComponent<DistractionMeter_Pendulum>();
    }
    // Update is called once per frame
    // void Update()
    // {
    //    switch(m_currentGameState)
    //    {
    //        case GameStates.NORMAL:
    //        print("STATE: NORMAL");
    //        #region Identifying the tail states
    //             KeyCode m_tailPulled =
    //              (Input.GetKeyDown(m_tailKeyOne) && !Input.GetKeyDown(m_tailKeyTwo))? m_tailKeyOne:
    //              (!Input.GetKeyDown(m_tailKeyOne) && Input.GetKeyDown(m_tailKeyTwo))? m_tailKeyTwo:
    //              (Input.GetKeyDown(m_tailKeyOne) && Input.GetKeyDown(m_tailKeyTwo))? m_tailKeyBoth:
    //              KeyCode.None;
    //         #endregion
    //        print(m_tailPulled);
    //        if(m_tailPulled != KeyCode.None)TailPulled(m_tailPulled);
    //        break;
    //        case GameStates.FIGHT:
    //        print("STATE: FIGHT");
    //        break;
    //    }
    // }
    // void UpdateGameState(GameStates p_state)
    // {
    //     m_currentGameState = p_state;
    // }
    // void UpdateActivePlayer(Player p_player)
    // {
    //     m_activePlayer = p_player;
    // }
    // void TailPulled(KeyCode m_input)
    // {
    //     print(m_input);
    //     BarArea.HIT t_hit = m_pendulum.GetHit();
    //     switch(m_input)
    //     {
    //         case m_tailKeyOne:

    //             if(m_activePlayer == Player.TWO || !m_playerTwo.GetComponent<Player_Spirit>().CanInteract(m_cat))break;
    //             if(m_activePlayer == Player.NONE)
    //             {
    //                 UpdateControll(Player.TWO);
    //                 break;
    //             }
    //             switch(t_hit)
    //             {
    //                 case BarArea.HIT.Bad:
    //                     m_pendulum.ModifyDistraction(-m_DistractionReduction);
    //                 break;
    //                 case BarArea.HIT.Good:
    //                     m_currentGameState = GameStates.FIGHT;
    //                 break;
    //                 case BarArea.HIT.Perfect:
    //                     UpdateControll(Player.TWO);
    //                 break;
    //             }
    //         break;
    //         case m_tailKeyTwo:
    //             if(m_activePlayer == Player.ONE || !m_playerOne.GetComponent<Player_Spirit>().CanInteract(m_cat))break;
    //             if(m_activePlayer == Player.NONE)
    //             {
    //                 UpdateControll(Player.ONE);
    //                 break;
    //             }
    //             switch(t_hit)
    //             {
    //                 case BarArea.HIT.Bad:
    //                     m_pendulum.ModifyDistraction(-m_DistractionReduction);
    //                 break;
    //                 case BarArea.HIT.Good:
    //                     m_currentGameState = GameStates.FIGHT;
    //                 break;
    //                 case BarArea.HIT.Perfect:
    //                     UpdateControll(Player.ONE);
    //                 break;
    //             }
    //         break;
    //         case m_tailKeyBoth:
    //             if( m_playerOne.GetComponent<Player_Spirit>().CanInteract(m_cat) &&
    //                 m_playerTwo.GetComponent<Player_Spirit>().CanInteract(m_cat))m_currentGameState = GameStates.FIGHT;
    //         break;
    //     }
    // }
    // ///<summary>
    // ///Updates the active player that's in control of the cat.
    // ///If PlayerFocus.None is sent in, the script ejects the current active player.
    // ///</summary>
    // void UpdateControll(Player m_player)
    // {   
    //     print(m_player);
    //     switch(m_player)
    //     {
    //         case Player.NONE:
    //         if(m_activePlayer == Player.NONE){break;}
    //             switch(m_activePlayer)
    //             {
    //                 case Player.ONE:
    //                 m_playerOne.GetComponent<Player_Spirit>().Launch(-Vector3.right);
    //                 break;
    //                 case Player.TWO:
    //                 m_playerTwo.GetComponent<Player_Spirit>().Launch(-Vector3.right);
    //                 break;
    //             }
    //             m_activePlayer = Player.NONE;
    //             m_cat.GetComponent<Cat>().SwitchActiveWalkControlls(KeyCode.None,KeyCode.None,1);
    //             m_pendulum.SetDistraction(m_DistractionReset);
    //         break;
    //         case Player.ONE:
    //         if(m_activePlayer == Player.ONE){break;}
    //             switch(m_activePlayer)
    //             {
    //                 case Player.TWO: m_playerTwo.GetComponent<Player_Spirit>().Launch(-Vector3.right);break;
    //             }
    //             m_activePlayer = Player.ONE;
    //             m_cat.GetComponent<Cat>().UpdateActivePlayer(m_playerOne);
    //             m_cat.GetComponent<Cat>().SwitchActiveWalkControlls(m_playerOne.GetComponent<Player_Spirit>().LeftKey,m_playerOne.GetComponent<Player_Spirit>().RightKey,1);
    //             m_pendulum.SetDistraction(m_DistractionReset);
    //         break;
    //         case Player.TWO:
    //          if(m_activePlayer == Player.TWO){break;}
    //             switch(m_activePlayer)
    //             {
    //                 case Player.ONE: m_playerOne.GetComponent<Player_Spirit>().Launch(-Vector3.right);break;
    //             }
    //             m_activePlayer = Player.TWO;
    //             m_cat.GetComponent<Cat>().UpdateActivePlayer(m_playerTwo);
    //             m_cat.GetComponent<Cat>().SwitchActiveWalkControlls(m_playerTwo.GetComponent<Player_Spirit>().LeftKey,m_playerTwo.GetComponent<Player_Spirit>().RightKey,1);
    //             m_pendulum.SetDistraction(m_DistractionReset);
    //         break;
    //     }
    // }
}

