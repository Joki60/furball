﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;
using static Player_Manager;

public class TextHandler : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI[] m_textFields;
    int[] m_collumn;

    [SerializeField]
    RectTransform m_selectionUI; 
    int m_currentSelection_row = 0;
    int m_currentSelection_collumn = 0;
    private char[] m_chars = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};

    #region KEYCODES
    KeyCode m_playerOne_Left = KeyCode.A;
    KeyCode m_playerOne_Right = KeyCode.D;
    KeyCode m_playerOne_Select = KeyCode.Q;
    KeyCode m_playerTwo_Left = KeyCode.Z;
    KeyCode m_playerTwo_right = KeyCode.C;
    KeyCode m_playerTwo_Select = KeyCode.E;
    [HideInInspector]
    public KeyCode m_left;
    [HideInInspector]
    public KeyCode m_right;
    [HideInInspector]
    public KeyCode m_forwards;
    [HideInInspector]
    public KeyCode m_backwards;
    [HideInInspector]
    public bool m_show = false;
    #endregion
    #region Image
    [SerializeField]
    Image m_upImage;
    [SerializeField]
    Sprite m_upSprite_Pink;
    [SerializeField]
    Sprite m_upSprite_Blue;
    [SerializeField]
    Image m_downImage;
    [SerializeField]
    Sprite m_downSprite_Pink;
    [SerializeField]
    Sprite m_downSprite_Blue;

    #endregion
    // Start is called before the first frame update
    void Start()
    {
        m_selectionUI.position = m_textFields[0].rectTransform.position; 
        m_collumn = new int[m_textFields.Length];
        

        m_left = (FindObjectOfType<VictoryHandler>().Winner == Player.ONE)? m_playerOne_Left:
                 (FindObjectOfType<VictoryHandler>().Winner == Player.TWO)? m_playerTwo_Left:
                 KeyCode.O;  

        m_right = (FindObjectOfType<VictoryHandler>().Winner == Player.ONE)? m_playerOne_Right:
                  (FindObjectOfType<VictoryHandler>().Winner == Player.TWO)? m_playerTwo_right:
                  KeyCode.P;  

        m_upImage.sprite = (FindObjectOfType<VictoryHandler>().Winner == Player.ONE)? m_upSprite_Pink: m_upSprite_Blue;
        m_downImage.sprite = (FindObjectOfType<VictoryHandler>().Winner == Player.ONE)? m_downSprite_Pink: m_downSprite_Blue;
        
        m_forwards = m_playerOne_Select;
        m_backwards = m_playerTwo_Select;
    }
    // Update is called once per frame
    void Update()
    {
        switch(m_show)
        {
            case true:
                foreach(TextMeshProUGUI text in m_textFields)
                {
                    text.enabled = true;
                }
                m_selectionUI.gameObject.SetActive(true);
            break;
            case false:
                foreach(TextMeshProUGUI text in m_textFields)
                {
                    text.enabled = false;
                }
                m_selectionUI.gameObject.SetActive(false);
            break;
        }
        if(m_show == false){return;}

        #region CheckingInputs
        m_currentSelection_row = (Input.GetKeyDown(m_forwards))? m_currentSelection_row +1: (Input.GetKeyDown(m_backwards) && m_currentSelection_row > 0)? m_currentSelection_row -1 : m_currentSelection_row;
        #endregion
        if(m_currentSelection_row > m_collumn.Length-1){ FindObjectOfType<VictoryHandler>().SerializeScore(AppendName());return;}

        m_collumn[m_currentSelection_row] = (Input.GetKeyDown(m_left))? m_collumn[m_currentSelection_row] -1: (Input.GetKeyDown(m_right))?m_collumn[m_currentSelection_row] + 1: m_collumn[m_currentSelection_row];
        m_collumn[m_currentSelection_row] = (m_collumn[m_currentSelection_row] < 0)? m_chars.Length-1:(m_collumn[m_currentSelection_row] > m_chars.Length-1)? 0: m_collumn[m_currentSelection_row];
        
        
        UpdateTextField(m_currentSelection_row,m_collumn[m_currentSelection_row]);
        
    }
    private void UpdateTextField(int row, int collumn)
    {
        int left = (collumn == 0)? m_chars.Length-1: collumn - 1;
        int mid = collumn;
        int right = (collumn == m_chars.Length-1)? 0: collumn + 1;

        m_textFields[row].text = m_chars[left] + "\n" + m_chars[mid] + "\n" + m_chars[right];

        if(m_selectionUI.position != m_textFields[row].rectTransform.position){m_selectionUI.position = m_textFields[row].rectTransform.position;}
    }
    private string AppendName()
    {
        string name = "";
        foreach (int collumn in m_collumn)
        {
            name += m_chars[collumn];
        }
        return name;  
    }
    
}
