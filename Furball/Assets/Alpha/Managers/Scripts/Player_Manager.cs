﻿using System.Collections;
using System.Collections.Generic;
using static Prompt_Manager;
using UnityEngine;

public class Player_Manager : MonoBehaviour
{
    public enum Player
    {
        NONE = 0,
        ONE,
        TWO,
        INTRANSITION,
    }
    Player m_activePlayer;
    public Player ActivePlayer{get{return m_activePlayer;}}

    #region Inputs
    const KeyCode m_EnterKeyOne = KeyCode.Q;
    const KeyCode m_EnterKeyTwo = KeyCode.E; 
    public KeyCode EnterKeyOne{get{return m_EnterKeyOne;}}
    public KeyCode EnterKeyTwo{get{return m_EnterKeyTwo;}}
    const KeyCode m_EnterKeyBoth = KeyCode.J;
    #endregion
    GameObject m_playerOne;
    GameObject m_playerTwo;
    GameObject m_Cat;

    #region ChargeBar
    float m_chargeAmount = 0;
    float m_chargeMax = 100;
    float m_chargeMultiplier = 4.5f;
    float m_chargeMultiplier_overtime = 5f;
    float[] m_chargeReset = {50,65,75,90};
    int m_chargeAttemts = 0;
    public float Charge{get{return m_chargeAmount;}}
    #endregion
    Prompt_Manager m_promptManager;
    #region Prompts
    bool m_playerRun_One = false;
    bool m_playerRun_Two = false;
    float m_spinTheBallTimer = 0;
    float m_spinTheBallTime = 2;

    #endregion
    
    // Start is called before the first frame update
    void Start()
    {
        m_Cat = GameObject.FindGameObjectWithTag("Cat");
        m_playerOne = GameObject.FindGameObjectWithTag("Player1");
        m_playerTwo = GameObject.FindGameObjectWithTag("Player2");
        m_promptManager = FindObjectOfType<Prompt_Manager>();
        
    }
    // Update is called once per frame
    void Update()
    {
        if(GameObject.FindObjectOfType<Menu_Manager>().State != Menu_Manager.States.DEFAULT){return;}

        //if(m_activePlayer == Player.NONE){m_promptManager.Display(Prompts.PRESSBUTTON_ONE,0.5f);m_promptManager.Display(Prompts.PRESSBUTTON_TWO,0.5f);}
        switch(m_activePlayer)
        {
            case Player.NONE:
            m_promptManager.Display(Prompts.PRESSBUTTON_ONE,0.5f);
            m_promptManager.Display(Prompts.PRESSBUTTON_TWO,0.5f);
            break;
            case Player.ONE:
            if(m_chargeAmount >= m_chargeMax){m_promptManager.Display(Prompts.PRESSBUTTON_TWO,0.5f);}
            else if(m_spinTheBallTimer >= m_spinTheBallTime){m_promptManager.Display(Prompts.SPINTHEBALL_TWO,0.5f);}
            break;
            case Player.TWO:
            if(m_chargeAmount >= m_chargeMax){m_promptManager.Display(Prompts.PRESSBUTTON_ONE,0.5f);}
            else if(m_spinTheBallTimer >= m_spinTheBallTime){m_promptManager.Display(Prompts.SPINTHEBALL_ONE,0.5f);}
            break;
        }
        if(m_spinTheBallTimer < m_spinTheBallTime && m_activePlayer != Player.NONE){m_spinTheBallTimer += Time.deltaTime;}

        if(m_activePlayer != Player.NONE){UpdateChargeMeter();}

        if(m_chargeAmount == m_chargeMax && (m_activePlayer == Player.ONE && Input.GetKeyDown(m_EnterKeyOne)))
        {
            m_chargeAmount = m_chargeReset[m_chargeAttemts];
            m_chargeAttemts = (m_chargeAttemts < m_chargeReset.Length - 1)? m_chargeAttemts + 1: m_chargeAttemts;
        }

        else if(m_chargeAmount == m_chargeMax && (m_activePlayer == Player.TWO && Input.GetKeyDown(m_EnterKeyTwo)))
        {
            m_chargeAmount = m_chargeReset[m_chargeAttemts];
            m_chargeAttemts = (m_chargeAttemts < m_chargeReset.Length - 1)? m_chargeAttemts + 1: m_chargeAttemts;
        }

        KeyCode t_input = (Input.GetKeyDown(m_EnterKeyOne) && Input.GetKeyDown(m_EnterKeyTwo))? m_EnterKeyBoth:
                          (Input.GetKeyDown(m_EnterKeyOne))? m_EnterKeyOne:
                          (Input.GetKeyDown(m_EnterKeyTwo))? m_EnterKeyTwo:
                          KeyCode.None; 

        switch(t_input)
        {
            case m_EnterKeyOne:
                if(m_activePlayer == Player.ONE || m_activePlayer == Player.INTRANSITION || (m_chargeAmount < m_chargeMax && m_activePlayer != Player.NONE) || m_playerOne.GetComponent<Player_Spirit>().InTransition){break;}
                UpdateActive(Player.ONE);
            break;
            case m_EnterKeyTwo:
                if(m_activePlayer == Player.TWO || m_activePlayer == Player.INTRANSITION|| (m_chargeAmount < m_chargeMax && m_activePlayer != Player.NONE) || m_playerTwo.GetComponent<Player_Spirit>().InTransition){break;}
                UpdateActive(Player.TWO);
            break;
            case m_EnterKeyBoth:
                if(m_activePlayer == Player.TWO  && m_chargeAmount >= m_chargeMax || m_playerOne.GetComponent<Player_Spirit>().InTransition)
                {
                    UpdateActive(Player.ONE);
                }
                else if(m_activePlayer == Player.ONE && m_chargeAmount >= m_chargeMax || m_playerTwo.GetComponent<Player_Spirit>().InTransition)
                {
                    UpdateActive(Player.TWO);
                }
            break;
        }
        
        
    }
    public void UpdateActive(Player m_player)
    {
        m_Cat.GetComponent<Cat>().SwitchActiveWalkControlls(KeyCode.None,KeyCode.None);
        m_chargeAmount = 0;
        m_chargeAttemts = 0;
        m_spinTheBallTimer = 0;
        switch(m_player)
        {
            case Player.ONE:
                if(m_activePlayer == Player.TWO)
                {
                    m_playerTwo.GetComponent<Player_Spirit>().Launch();
                }
                m_Cat.GetComponent<Cat>().UpdateActivePlayer(m_playerOne);
                 m_activePlayer = Player.INTRANSITION;
            break;
            case Player.TWO:
                if(m_activePlayer == Player.ONE)
                {
                    m_playerOne.GetComponent<Player_Spirit>().Launch();
                }
                m_Cat.GetComponent<Cat>().UpdateActivePlayer(m_playerTwo);
                 m_activePlayer = Player.INTRANSITION;
            break;
            case Player.NONE:
                if(m_activePlayer == Player.TWO)
                {
                    m_playerTwo.GetComponent<Player_Spirit>().Launch();
                }
                if(m_activePlayer == Player.ONE)
                {
                    m_playerOne.GetComponent<Player_Spirit>().Launch();
                }
                m_activePlayer = Player.NONE;
            break;
        }
    }
    void UpdateChargeMeter()
    {
        if(Mathf.Abs(Input.GetAxis("Mouse X")) + Mathf.Abs(Input.GetAxis("Mouse Y")) > 0){m_spinTheBallTimer = 0;}
        m_chargeAmount += (Mathf.Abs(Input.GetAxis("Mouse X")) + Mathf.Abs(Input.GetAxis("Mouse Y"))) * Time.deltaTime * m_chargeMultiplier + (Time.deltaTime * m_chargeMultiplier_overtime);
        m_chargeAmount = (m_chargeAmount > m_chargeMax)? m_chargeMax : m_chargeAmount;
    }
    public void UpdateActivePlayer(Player p_activePlayer)
    {
        m_activePlayer = p_activePlayer;
        if(!m_playerRun_One && p_activePlayer == Player.ONE)
        {
            m_playerRun_One = true;
            m_promptManager.Display(Prompts.RUN_ONE,1);
        }
        else if(!m_playerRun_Two && p_activePlayer == Player.TWO)
        {
            m_playerRun_Two = true;
            m_promptManager.Display(Prompts.RUN_TWO,1);
        }
        else
        {
            switch(m_activePlayer)
            {
                case Player.ONE:
                m_promptManager.Display(Prompts.SWITCH_ONE,0.5f);
                break;
                case Player.TWO:
                m_promptManager.Display(Prompts.SWITCH_TWO,0.5f);
                break;
            }
        }
    }
}
