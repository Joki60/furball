﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Player_Manager;
using static Variables;
using static Prompt_Manager;
using TMPro;
using UnityEngine.SceneManagement;
using static EasingFunction;

public class VictoryHandler : MonoBehaviour
{
    private Player m_winner = Player.ONE;
    public Player Winner
    {
        get{return m_winner;}
    }
    [SerializeField]
    private TextMeshProUGUI m_highScore;
    [SerializeField]
    private Variables data;
    States m_state = States.CALCULATESCORE;


    #region Text Display
    [SerializeField]
    TextMeshProUGUI m_scoreText_One;
    [SerializeField]
    TextMeshProUGUI m_scoreText_Two;
    float[] m_scoreDisplayTime = {6,4};
    float[] m_scoreDisplayTimer  = {0,0};
    float m_startWaitTime = 2;
    float m_startWaitTimer = 0;
    float m_animationFadeTime = 1.2f;
    float m_animationFadeTimer = 0;
    float m_interMediateTimer = 0;
    float[] m_interMediateTime = {0.2f,0.2f,0.6f};
    int m_winnerAnimationState = 0;
    bool m_winnerAnimationFinished = false;
    bool m_enableFade = false;
    EasingFunction.Ease m_textEasing = EasingFunction.Ease.EaseOutQuint;
    EasingFunction.Function m_textFunction;
    #endregion

    #region Animator
    [SerializeField]
    Animator m_scoreAnim;
    [SerializeField]
    Animator m_nameAnim;
    [SerializeField]
    Animator m_highScoreAnim;
    [SerializeField]
    Animator m_catAnim;
    #endregion
    float m_endScreenStayTime = 15f;
    float m_endScreenStayTimer = 0;

    public enum States
    {
        CALCULATESCORE,
        DISPLAYWINNER,
        ENTERNAME,
        DISPLAYSCORE,
    }


    void Awake()
    {
       Time.timeScale = 1;
       m_textFunction = EasingFunction.GetEasingFunction(m_textEasing);
    //    print(m_variables.one_score_GET_TOTAL());
    //    print(m_variables.two_score_GET_TOTAL());
       m_winner = (data.one_score_GET_TOTAL() > data.two_score_GET_TOTAL())? Player.ONE: (data.one_score_GET_TOTAL() < data.two_score_GET_TOTAL())? Player.TWO: Player.NONE;
       m_scoreAnim.SetBool("isActive",true);
       //NullHighScore();
      
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R)){data.Reset();SceneManager.LoadScene(1);}

        switch(m_state)
        {
            case States.CALCULATESCORE:
                if(m_startWaitTimer < m_startWaitTime){m_startWaitTimer += Time.deltaTime; return;}

                m_scoreDisplayTimer[0] = (m_scoreDisplayTimer[0] < m_scoreDisplayTime[0])? m_scoreDisplayTimer[0] + Time.deltaTime: 
                                         (m_scoreDisplayTimer[0] > m_scoreDisplayTime[0])? m_scoreDisplayTime[0]:
                                          m_scoreDisplayTimer[0];

                m_scoreDisplayTimer[1] = (m_scoreDisplayTimer[1] < m_scoreDisplayTime[1])? m_scoreDisplayTimer[1] + Time.deltaTime: 
                                         (m_scoreDisplayTimer[1] > m_scoreDisplayTime[1])? m_scoreDisplayTime[1]:
                                          m_scoreDisplayTimer[1];
                switch(m_winner)
                {
                    case Player.NONE:
                        m_scoreText_Two.text = "" + (int)m_textFunction(0,data.two_score_GET_TOTAL(), m_scoreDisplayTimer[0]/ m_scoreDisplayTime[0]);
                        m_scoreText_One.text = "" + (int)m_textFunction(0,data.one_score_GET_TOTAL(), m_scoreDisplayTimer[0]/ m_scoreDisplayTime[0]);
                    break;
                    case Player.ONE:
                        m_scoreText_One.text = "" + (int)m_textFunction(0,data.one_score_GET_TOTAL(), m_scoreDisplayTimer[0]/ m_scoreDisplayTime[0]);
                        m_scoreText_Two.text = "" + (int)m_textFunction(0,data.two_score_GET_TOTAL(), m_scoreDisplayTimer[1]/ m_scoreDisplayTime[1]);
                    break;
                    case Player.TWO:
                        m_scoreText_Two.text = "" + (int)m_textFunction(0,data.two_score_GET_TOTAL(), m_scoreDisplayTimer[0]/ m_scoreDisplayTime[0]);
                        m_scoreText_One.text = "" + (int)m_textFunction(0,data.one_score_GET_TOTAL(), m_scoreDisplayTimer[1]/ m_scoreDisplayTime[1]);
                    break;
                }
                if(m_scoreDisplayTimer[0] > m_scoreDisplayTime[0]){m_state = States.DISPLAYWINNER;m_catAnim.SetBool("isActive",true);}
            break;
            case States.DISPLAYWINNER: 
                Prompts m_prompt = (m_winner == Player.ONE)? Prompts.WIN_ONE:(m_winner == Player.TWO)? Prompts.WIN_TWO: Prompts.WIN_EVERONE;
                //FindObjectOfType<Prompt_Manager>().Display(m_prompt,0.6f);
                if(!m_enableFade){
                switch(m_winnerAnimationState)
                {
                    case 0:
                    FindObjectOfType<Prompt_Manager>().Display(Prompts.RING_ONE,0.6f);
                    m_interMediateTimer += Time.deltaTime;
                    if(m_interMediateTimer > m_interMediateTime[0]){m_interMediateTimer = 0; m_winnerAnimationState = 1;}
                    break;
                    case 1:
                    FindObjectOfType<Prompt_Manager>().Display(Prompts.RING_ONE,0.6f);
                    FindObjectOfType<Prompt_Manager>().Display(Prompts.RING_TWO,0.6f);
                    m_interMediateTimer += Time.deltaTime;
                    if(m_interMediateTimer > m_interMediateTime[1]){m_interMediateTimer = 0; m_winnerAnimationState = 2;}
                    break;
                    case 2:
                    FindObjectOfType<Prompt_Manager>().Display(Prompts.RING_ONE,0.6f);
                    FindObjectOfType<Prompt_Manager>().Display(Prompts.RING_TWO,0.6f);
                    FindObjectOfType<Prompt_Manager>().Display(m_prompt,0.6f);

                            switch(m_winner)
                            {
                                case Player.ONE:
                                if(IsHighScore(data.one_score_GET_TOTAL())){FindObjectOfType<Prompt_Manager>().Display(Prompts.HIGHSCORE_ONE,0.6f);}
                                print("UHM HELLO");
                                break;
                                case Player.TWO:
                                if(IsHighScore(data.two_score_GET_TOTAL())){FindObjectOfType<Prompt_Manager>().Display(Prompts.HIGHSCORE_TWO,0.6f);}
                                break;
                            }


                    m_interMediateTimer += Time.deltaTime;
                    if(m_interMediateTimer > m_interMediateTime[2]){m_winnerAnimationFinished = true;}
                    break;

                }
                }
                if(!m_winnerAnimationFinished){return;}
                if(Input.GetKeyDown(FindObjectOfType<TextHandler>().m_forwards) ||Input.GetKeyDown(FindObjectOfType<TextHandler>().m_backwards)){m_enableFade = true;}
                if(!m_enableFade){return;}
                if(m_animationFadeTimer < m_animationFadeTime){m_animationFadeTimer += Time.deltaTime;return;}
                m_scoreAnim.SetBool("isActive",false);
                switch(m_winner)
                {
                    case Player.NONE:
                       
                             m_state = States.DISPLAYSCORE;
                             m_highScoreAnim.SetBool("isActive",true);
                             m_catAnim.SetBool("isActive",false);
                    break; 
                    case Player.ONE:
                    case Player.TWO:
                            if(!IsHighScore(data.two_score_GET_TOTAL())){m_state = States.DISPLAYSCORE;m_highScoreAnim.SetBool("isActive",true);break;}
                            m_state = States.ENTERNAME;
                            m_enableFade = true;
                            m_catAnim.SetBool("isActive",false);
                        
                    break;
                }
                
            break;
            case States.ENTERNAME:
            if(FindObjectOfType<TextHandler>().m_show == false){FindObjectOfType<TextHandler>().m_show = true;}
            m_nameAnim.SetBool("isActive",true);
            //THIS IS BASICALLY FILLER
            break;
            case States.DISPLAYSCORE:
            if(!m_highScore.enabled){m_highScore.enabled = true; PrintHighScore();}
            m_endScreenStayTimer += Time.deltaTime;

            if(m_endScreenStayTimer > m_endScreenStayTime)
            {
                data.Reset();
                SceneManager.LoadScene(1);
            }

            break;
        }
    }
    private bool IsHighScore(int score)
    {
        print("Score; " + score);
        print("HIghscore: " + PlayerPrefs.GetInt("10"));
        return (score > PlayerPrefs.GetInt("10"));   
    }
    private void PrintHighScore()
    {
        
        m_highScore.text = "";
        for(int i = 1; i < 11; i++)
        {
            if(i != 1)
            {
                m_highScore.text += "\n" + i + "." + " NAME: " + PlayerPrefs.GetString("n"+ i) + " Score: " + PlayerPrefs.GetInt(""+ i);
                print(m_highScore.text);
                continue;
            }
            m_highScore.text += i + "." + " NAME: " + PlayerPrefs.GetString("n"+ i) + " Score: " + PlayerPrefs.GetInt(""+ i);
            print(m_highScore.text);
        }
    }
    private void SerializeHighScore(int score,string name)
    {
        print("Score: " + score + ", Name: " + name);
        int[] m_savedScore = new int[10];
        string[] m_savedName = new string[10];
        for (int i = 1; i < 11; i++)
        {
            m_savedScore[i-1] = PlayerPrefs.GetInt("" + i);
            m_savedName[i-1] = PlayerPrefs.GetString("n" + i);
        }
        for(int i = m_savedScore.Length; i > 0; i--)
        {   
            if(i == 1)
            {
                PlayerPrefs.SetInt("" + i,score);
                PlayerPrefs.SetString("n" + i,name);
                for(int x = 2; x < m_savedScore.Length+1; x++)
                {
                    PlayerPrefs.SetInt("" + x , m_savedScore[x-2]);
                    PlayerPrefs.SetString("n" + x, m_savedName[x-2]);
                }  
             break;
            }
            else if(score >= m_savedScore[i-1] && score <= m_savedScore[i-2]) 
            {   
                PlayerPrefs.SetInt("" + i, score);
                PlayerPrefs.SetString("n" + i,name);
                for(int x = i + 1; x < m_savedScore.Length +1; x++)
                {
                    PlayerPrefs.SetInt("" + x, m_savedScore[x-2]);
                    PlayerPrefs.SetString("n" + x, m_savedName[x-2]);
                }  
                break;    
            }
            else if(PlayerPrefs.GetInt("" + i) > score){break;}
        }
    }
    public void SerializeScore(string name)
    {
        int score = 0;
        switch(m_winner)
        {
            case Player.ONE:
                score = data.one_score_GET_TOTAL();
            break;
            case Player.TWO:
                score = data.two_score_GET_TOTAL();
            break;
        }
        SerializeHighScore(score,name);
        m_state = States.DISPLAYSCORE;
        m_nameAnim.SetBool("isActive", false);
        m_highScoreAnim.SetBool("isActive",true);
    }
    private void NullHighScore()
    {
        for(int i = 1; i < 11; i++)
        {
            PlayerPrefs.SetInt("" + i, 0);
            PlayerPrefs.SetString("n" + i,"AAA");
            print(PlayerPrefs.GetInt("" + i));
        }
    }
}
