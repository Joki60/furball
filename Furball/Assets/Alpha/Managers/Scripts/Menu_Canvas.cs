﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu_Canvas : MonoBehaviour
{
    // Start is called before the first frame update
    public void FadeInDone()
    {
        GameObject.FindObjectOfType<Menu_Manager>().ChangeState(Menu_Manager.States.GAMEPLAYMENUACTIVE);
    }
    public void FadeOutDone()
    {
        GameObject.FindObjectOfType<Menu_Manager>().ChangeState(Menu_Manager.States.DEFAULT);
    }
}
