﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using static Prompt_Manager;
using UnityEngine;

public class Menu_Manager : MonoBehaviour
{
    [SerializeField]
    Animator m_gameplayMenu_anim;
    [SerializeField]
    Animator m_leftPawPink_anim;
    [SerializeField]
    Animator m_rightPawPink_anim;
    [SerializeField]
    Animator m_leftPawBlue_anim;
    [SerializeField]
    Animator m_rightPawBlue_anim;
    [SerializeField]
    Animator m_buttonPink_anim;
    [SerializeField]
    Animator m_buttonBlue_anim;
    [SerializeField]
    GameObject m_UI;
    States m_menuState = States.GAMEPLAYMENUSTART;
    public States State{get{return m_menuState;}}

    //Blue is player two, Pink is player one
    bool m_leftPressed_Blue = false;
    bool m_rightPressed_Blue = false;
    bool m_leftPressed_Pink = false;
    bool m_rightPressed_Pink = false;
    bool m_buttonPressed_Blue = false;
    bool m_buttonPressed_Pink = false;
    bool m_enableButton_Pink = false;
    bool m_enableButton_Blue = false;

    KeyCode m_left_blue;
    KeyCode m_right_blue;
    KeyCode m_left_pink;
    KeyCode m_right_pink;
    KeyCode m_button_blue;
    KeyCode m_button_pink;

    float m_fadeOutTime = 2f;
    float m_fadeOutTimer;
    float m_rsgTime = 1.5f;
    float m_rsgTimer = 0;
    float m_rsgState = -1;
    Prompt_Manager m_promptManager;

    [SerializeField]
    Variables data;


    public enum States
    {
        DEFAULT,
        GAMEPLAYMENUSTART,
        GAMEPLAYMENUACTIVE,
        GAMEPLAYMENUFADEOUT,
        READYSETGO
    }

    void Start()
    {
        Cursor.visible = false;
        m_promptManager = FindObjectOfType<Prompt_Manager>();
        m_left_blue = GameObject.FindGameObjectWithTag("Player2").GetComponent<Player_Spirit>().LeftKey;
        m_right_blue = GameObject.FindGameObjectWithTag("Player2").GetComponent<Player_Spirit>().RightKey;
        m_button_blue = FindObjectOfType<Player_Manager>().EnterKeyTwo;
        m_left_pink = GameObject.FindGameObjectWithTag("Player1").GetComponent<Player_Spirit>().LeftKey;
        m_right_pink = GameObject.FindGameObjectWithTag("Player1").GetComponent<Player_Spirit>().RightKey;
        m_button_pink = FindObjectOfType<Player_Manager>().EnterKeyOne;

        switch(data.Round)
        {
            case 1:
            m_gameplayMenu_anim.SetBool("isActive",true);
            m_UI.SetActive(false);
            m_menuState = States.GAMEPLAYMENUACTIVE;
            break;
            case 2:
            case 3:
            m_gameplayMenu_anim.SetBool("isActive",false);
            m_UI.SetActive(true);
            m_menuState = States.READYSETGO;
            break;
        }
    }

    // Update is called once per frame
    void Update()
    {
    if(Input.GetKeyDown(KeyCode.R)){data.Reset();SceneManager.LoadScene(1);}

    if(m_menuState == States.GAMEPLAYMENUACTIVE)
    {



        #region Checking for inputs
            
            m_leftPressed_Blue = (!m_leftPressed_Blue && Input.GetKeyDown(m_left_blue))? true:(!m_buttonPressed_Blue && m_leftPressed_Blue && Input.GetKeyUp(m_left_blue))? false: m_leftPressed_Blue;
            m_rightPressed_Blue = (!m_rightPressed_Blue && Input.GetKeyDown(m_right_blue))? true:(!m_buttonPressed_Blue && m_rightPressed_Blue && Input.GetKeyUp(m_right_blue))? false: m_rightPressed_Blue;
            m_leftPressed_Pink = (!m_leftPressed_Pink && Input.GetKeyDown(m_left_pink))? true:(!m_buttonPressed_Pink && m_leftPressed_Pink && Input.GetKeyUp(m_left_pink))? false: m_leftPressed_Pink;
            m_rightPressed_Pink = (!m_rightPressed_Pink && Input.GetKeyDown(m_right_pink))? true:(!m_buttonPressed_Pink && m_rightPressed_Pink && Input.GetKeyUp(m_right_pink))? false: m_rightPressed_Pink;

            m_enableButton_Blue = (m_leftPressed_Blue && m_rightPressed_Blue);
            m_enableButton_Pink = (m_leftPressed_Pink && m_rightPressed_Pink);

            m_buttonPressed_Blue = (!m_buttonPressed_Blue && Input.GetKeyDown(m_button_blue) && m_enableButton_Blue)? true:(!m_enableButton_Blue)? false: m_buttonPressed_Blue;
            m_buttonPressed_Pink = (!m_buttonPressed_Pink && Input.GetKeyDown(m_button_pink) && m_enableButton_Pink)? true:(!m_enableButton_Pink)? false: m_buttonPressed_Pink;
        #endregion

            m_leftPawBlue_anim.SetBool("isActive",m_leftPressed_Blue);
            m_rightPawBlue_anim.SetBool("isActive",m_rightPressed_Blue);
            m_leftPawPink_anim.SetBool("isActive",m_leftPressed_Pink);
            m_rightPawPink_anim.SetBool("isActive",m_rightPressed_Pink);

            m_buttonBlue_anim.SetBool("isVisible",m_enableButton_Blue);
            m_buttonBlue_anim.SetBool("isActive", m_buttonPressed_Blue);
            m_buttonPink_anim.SetBool("isVisible",m_enableButton_Pink);
            m_buttonPink_anim.SetBool("isActive", m_buttonPressed_Pink);

            if(m_enableButton_Blue && !m_buttonPressed_Blue)
            {
                FindObjectOfType<Prompt_Manager>().Display(Prompt_Manager.Prompts.READY_TWO,0.6f);
            }
            if(m_enableButton_Pink && !m_buttonPressed_Pink)
            {
                FindObjectOfType<Prompt_Manager>().Display(Prompt_Manager.Prompts.READY_ONE,0.6f);
            }

            if( m_buttonPressed_Blue && m_buttonPressed_Pink || Input.GetKeyDown(KeyCode.S))
            {
                m_menuState = States.GAMEPLAYMENUFADEOUT;
                m_buttonBlue_anim.SetBool("isVisible",false);
                m_buttonPink_anim.SetBool("isVisible", false);
                m_leftPawBlue_anim.SetBool("isVisible",false);
                m_rightPawBlue_anim.SetBool("isVisible",false);
                m_leftPawPink_anim.SetBool("isVisible",false);
                m_rightPawPink_anim.SetBool("isVisible",false);
                m_buttonBlue_anim.SetBool("isActive",false);
                m_buttonPink_anim.SetBool("isActive", false);
                m_leftPawBlue_anim.SetBool("isActive",false);
                m_rightPawBlue_anim.SetBool("isActive",false);
                m_leftPawPink_anim.SetBool("isActive",false);
                m_gameplayMenu_anim.SetBool("isActive",false);
            }
    }
    if(m_menuState == States.GAMEPLAYMENUFADEOUT)
    {
        m_fadeOutTimer += Time.deltaTime;
        if(m_fadeOutTimer > m_fadeOutTime)
        {
            m_fadeOutTimer = 0;
            m_UI.SetActive(true);
            m_menuState = States.READYSETGO;
        }
    }
    if(m_menuState == States.READYSETGO)
    {
        switch(m_rsgState)
        {
            case -1:
                m_rsgTimer += Time.deltaTime;
                if(m_rsgTimer > 0.2f)
                {
                    switch(data.Round)
                    {
                        case 1:
                        m_promptManager.Display(Prompts.ROUND_ONE,1f);
                        break;
                        case 2:
                        m_promptManager.Display(Prompts.ROUND_TWO,1f);
                        break;
                        case 3:
                        m_promptManager.Display(Prompts.ROUND_FINAL,1f);
                        break;
                    }
                    m_rsgTimer = 0;
                    m_rsgState = 0;
                }
            break;
            case 0:
                m_rsgTimer += Time.deltaTime;
                if(m_rsgTimer > m_rsgTime)
                {
                    m_promptManager.Display(Prompts.READY,1f);
                    m_rsgTimer = 0;
                    m_rsgState = 1;
                }
            break;
            case 1:
                m_rsgTimer += Time.deltaTime;
                if(m_rsgTimer > m_rsgTime)
                {
                    m_promptManager.Display(Prompts.SET,1f);
                    m_rsgTimer = 0;
                    m_rsgState = 2;
                }
            break;
            case 2:
                m_rsgTimer += Time.deltaTime;
                if(m_rsgTimer > m_rsgTime)
                {
                    m_promptManager.Display(Prompts.GO,0.6f);
                    m_rsgTimer = 0;
                    m_rsgState = -1;
                    m_menuState = States.DEFAULT;
                }
            break;
        }

    }

    }
    public void ChangeState(States p_state)
    {
        m_menuState = p_state;
        if(p_state == States.GAMEPLAYMENUACTIVE)
        {
            m_leftPawBlue_anim.SetBool("isVisible",true);
            m_rightPawBlue_anim.SetBool("isVisible",true);
            m_leftPawPink_anim.SetBool("isVisible",true);
            m_rightPawPink_anim.SetBool("isVisible",true);
        }
    }

}
