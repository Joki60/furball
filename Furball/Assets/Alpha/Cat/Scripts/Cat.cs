﻿using System.Collections;
using System.Collections.Generic;
using RavingBots.MultiInput;
using UnityEngine;
using static Player_Manager;

public class Cat : MonoBehaviour
{
    public AudioClip m_jumpAudio; 
    public AudioClip m_stepAudio;
    [SerializeField]
    public IDevice m_decive;
    [Header("Movement")]
    [SerializeField]
    float m_forwardForce;
    [SerializeField]
    float m_upwardsForce;
    [SerializeField]
    float m_forwardVelocityMax;
    [SerializeField]
    float m_upwardsVelocityMax;
    [SerializeField]
    float m_isGroundedDistance;
    [SerializeField]
    Transform m_isGroundedOrigin;
    Rigidbody2D m_rb;
    KeyCode m_activeInputWalkLeft;
    KeyCode m_activeInputWalkRight;
    private bool m_leftInput = false;
    private bool m_rightInput = false;
    private bool m_tempLeft = false;
    private bool m_tempRight = false;
    private float m_inputWaitTime = 0.04f;
    private float m_inputWaitTimer;
    private InputState m_inputstate = InputState.WAITFORINPUT;
    KeyCode m_activeInputWalkLast = KeyCode.None;
    Player m_activePlayer;
    [Header("Spirit interactions")]
    [SerializeField]
    public Transform m_anchor_PlayerOne;
    [SerializeField]
    public Transform m_anchor_PlayerTwo;
    //----- Used for walkanimation /mvh Elly -----
    Animator m_anim; 
    //-----
    [SerializeField]
    LayerMask m_jumpMask;
    [Header("Collision detection")]
    #region Raycasts
    [SerializeField]
    Vector3 m_raycastSpacing;
    [SerializeField]
    Vector3 m_raycatsOffset;
    [SerializeField]
    Vector3 m_raycastOriginOffset;
    [SerializeField]
    float m_rayLength;
    [SerializeField]
    LayerMask m_collidableObjectMask;
    #endregion
    [Header("Animation")]
    [SerializeField]
    Animator m_catHead_anim;
    [SerializeField]
    Animator m_glow_anim;
    private enum InputState
    {
        WAITFORINPUT,
        WAITFORADITIONALINPUT,
        WAIT
    }

    void Start()
    {
        
        m_rb = this.GetComponent<Rigidbody2D>();
        //SwitchActiveWalkControlls(KeyCode.A,KeyCode.B,1);


        //----- Just getting the animator for the catwalk, don't worry about it :) /mvh Elly -----
        m_anim = this.GetComponent<Animator>();
        //-----
    }
    // Update is called once per frame
    void Update()
    {
        switch(m_inputstate)
        {
            case InputState.WAITFORINPUT:
            m_tempLeft = (!m_tempLeft && Input.GetKeyUp(m_activeInputWalkLeft))? true: m_tempLeft;
            m_tempRight = (!m_tempRight && Input.GetKeyUp(m_activeInputWalkRight))? true: m_tempRight;
            if(m_tempLeft || m_tempRight){ m_inputstate = InputState.WAITFORADITIONALINPUT;}
            break;
            case InputState.WAITFORADITIONALINPUT:
            m_inputWaitTimer += Time.deltaTime;
            m_tempLeft = (!m_tempLeft && Input.GetKeyUp(m_activeInputWalkLeft))? true: m_tempLeft;
            m_tempRight = (!m_tempRight && Input.GetKeyUp(m_activeInputWalkRight))? true: m_tempRight;
            if(m_inputWaitTimer > m_inputWaitTime ||(m_tempLeft && m_tempRight))
            {
                m_inputWaitTimer = 0; 
                m_inputstate = InputState.WAIT;
                m_leftInput = m_tempLeft;
                m_rightInput = m_tempRight;
            }
            break;
            case InputState.WAIT:break;
        }

        //------------For animation /mvh Elly
        if (Input.GetKeyUp(m_activeInputWalkLeft))
        {
            m_anim.SetBool("isLeftStep", false);
        }
        else
        {
            m_anim.SetBool("isLeftStep", true);
        }
        if (Input.GetKeyUp(m_activeInputWalkRight))
        {
            m_anim.SetBool("isRightStep", false);
        }
        else
        {
            m_anim.SetBool("isRightStep", true);
        }
        if (IsGrounded())
        {
            m_anim.SetBool("isGrounded", true);
        }
        if (!IsGrounded())
        {
            m_anim.SetBool("isGrounded", false);
        }

        //-------------

        switch (m_activePlayer)
        {
            case Player.ONE:
            if(m_catHead_anim.GetInteger("PlayerFocus") != 1){m_catHead_anim.SetInteger("PlayerFocus",1);}
            if(m_glow_anim.GetInteger("PlayerFocus") != 1){m_glow_anim.SetInteger("PlayerFocus",1);}
            break;
            case Player.TWO:
            if(m_catHead_anim.GetInteger("PlayerFocus") != 2){m_catHead_anim.SetInteger("PlayerFocus",2);}
            if(m_glow_anim.GetInteger("PlayerFocus") != 2){m_glow_anim.SetInteger("PlayerFocus",2);}
            break;
            case Player.NONE:
            if(m_catHead_anim.GetInteger("PlayerFocus") != 0){m_catHead_anim.SetInteger("PlayerFocus",0);}
            if(m_glow_anim.GetInteger("PlayerFocus") != 0){m_glow_anim.SetInteger("PlayerFocus",0);}
            break;
        }

    }
    void FixedUpdate()
    {
        if(m_leftInput && m_rightInput && IsGrounded())
        {
            m_rb.AddForce(Vector3.up * m_upwardsForce);
            GameObject.Find("Score_Manager").GetComponent<Score_Manager>().Add(m_activePlayer,1);
            SoundManager.instance.PlaySingle(m_jumpAudio);
            m_anim.SetTrigger("Jump");
            
        }

        else if(m_leftInput && (m_activeInputWalkLast == m_activeInputWalkRight || m_activeInputWalkLast == KeyCode.None))
        {
            m_rb.AddForce(Vector3.right * m_forwardForce);
            m_activeInputWalkLast = m_activeInputWalkLeft;
            GameObject.Find("Score_Manager").GetComponent<Score_Manager>().Add(m_activePlayer,1);
            m_activeInputWalkLast = m_activeInputWalkLeft;
        }
        else if(m_rightInput && (m_activeInputWalkLast == m_activeInputWalkLeft|| m_activeInputWalkLast == KeyCode.None))
        {
            m_rb.AddForce(Vector3.right * m_forwardForce);
            m_activeInputWalkLast = m_activeInputWalkRight;
            GameObject.Find("Score_Manager").GetComponent<Score_Manager>().Add(m_activePlayer,1);
            m_activeInputWalkLast = m_activeInputWalkRight;
        }
        if(m_leftInput || m_rightInput)
        {
            m_rightInput = false;
            m_leftInput = false;
            m_tempLeft = false;
            m_tempRight = false;
            m_inputstate = InputState.WAITFORINPUT;
        }
        
        #region Fixing The Velocities
            if(m_rb.velocity.x > m_forwardVelocityMax){m_rb.velocity = new Vector2(m_forwardVelocityMax, m_rb.velocity.y);}
            if(m_rb.velocity.y > m_upwardsVelocityMax){m_rb.velocity = new Vector2(m_rb.velocity.x,m_upwardsVelocityMax);}
        #endregion


    }
    public void SwitchActiveWalkControlls(KeyCode left, KeyCode right)
    {
        m_activeInputWalkLeft = left;
        m_activeInputWalkRight = right;
        m_activeInputWalkLast = KeyCode.None;
    }
    public void UpdateActivePlayer(GameObject player)
    {
        if(!player.GetComponent<Player_Spirit>())
        {
            print("No Player_Spirit attached");
            return;
        }
        m_activePlayer = player.GetComponent<Player_Spirit>().Player;
        switch(player.GetComponent<Player_Spirit>().Player)
        {
            case Player.ONE:
            player.GetComponent<Player_Spirit>().SnapTo(m_anchor_PlayerOne.position);
            break;
            case Player.TWO:
            player.GetComponent<Player_Spirit>().SnapTo(m_anchor_PlayerTwo.position);
            break;
        }
        //switch(player.GetComponent<Player_Spirit>().)
    }
    private bool IsGrounded()
    {
        return (Physics2D.Raycast(m_isGroundedOrigin.position,Vector3.down, m_isGroundedDistance, m_jumpMask) && m_rb.velocity.y <= 0);
    }
    private void OnDrawGizmosSelected()
    {
        Debug.DrawRay(m_isGroundedOrigin.position, Vector3.down * m_isGroundedDistance);

        //CollidableObjectsRaycasts
        for (int i = -1; i < 2; i++)
        {
            Vector3 downPosition = this.transform.position + m_raycastOriginOffset + new Vector3(0,-m_raycatsOffset.y) + new Vector3(m_raycastSpacing.x * i,0);
            Vector3 upPosition = this.transform.position + m_raycastOriginOffset + new Vector3(0,m_raycatsOffset.y) + new Vector3(m_raycastSpacing.x * i,0);
            Vector3 sidePosition = this.transform.position + m_raycastOriginOffset + new Vector3(m_raycatsOffset.x, 0) + new Vector3(0,m_raycastSpacing.y * i);
            Debug.DrawRay(downPosition,-Vector3.up * m_rayLength, Color.magenta);
            Debug.DrawRay(upPosition, Vector3.up * m_rayLength, Color.magenta);
            Debug.DrawRay(sidePosition, Vector3.right * m_rayLength,Color.magenta);        
        }
    }
    ///<Summary>
    ///Requires that the Gameobject has CollidableObject attached to it. 
    ///Checks if the object is colliding based on the CollidableObject.Directions variable
    public bool IsColliding(GameObject m_collidabeObject)
    {
        if(!m_collidabeObject.GetComponent<CollidableObject>()){return false;}

        CollidableObject.Directions[] t_direction = m_collidabeObject.GetComponent<CollidableObject>().m_Directions;

        foreach (CollidableObject.Directions direction in t_direction)
        {
            switch(direction)
            {
                case CollidableObject.Directions.ABOVE:
                    for (int i = -1; i < 2; i++)
                    {
                        Vector3 position = this.transform.position + m_raycastOriginOffset + new Vector3(0,-m_raycatsOffset.y) + new Vector3(m_raycastSpacing.x * i,0);
                        RaycastHit2D m_hit = Physics2D.Raycast(position, Vector3.up, m_rayLength, m_collidableObjectMask);
                        if(!m_hit){continue;}
                        if(m_hit.collider.gameObject.GetInstanceID() == m_collidabeObject.GetInstanceID()){return true;}
                    }
                break;
                case CollidableObject.Directions.SIDE:
                    for (int i = -1; i < 2; i++)
                    {
                        Vector3 position = this.transform.position + m_raycastOriginOffset + new Vector3(m_raycatsOffset.x, 0) + new Vector3(0,m_raycastSpacing.y * i);
                        RaycastHit2D m_hit = Physics2D.Raycast(position, Vector3.right, m_rayLength, m_collidableObjectMask);
                        if(!m_hit){continue;}
                        if(m_hit.collider.gameObject.GetInstanceID() == m_collidabeObject.GetInstanceID()){return true;}
                    }
                break;
                case CollidableObject.Directions.UNDER:
                    for (int i = -1; i < 2; i++)
                    {
                        Vector3 position = this.transform.position + m_raycastOriginOffset + new Vector3(0,m_raycatsOffset.y) + new Vector3(m_raycastSpacing.x * i,0);
                        RaycastHit2D m_hit = Physics2D.Raycast(position, -Vector3.up, m_rayLength, m_collidableObjectMask);
                        if(!m_hit){continue;}
                        if(m_hit.collider.gameObject.GetInstanceID() == m_collidabeObject.GetInstanceID()){return true;}
                    }
                break;
            }
        }   
        return false;
    }
    void Step()
    {
       FindObjectOfType<SoundManager>().PlaySingle(m_stepAudio);
    }
}
