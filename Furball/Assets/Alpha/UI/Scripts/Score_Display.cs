﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[RequireComponent(typeof(Text))]
public class Score_Display : MonoBehaviour
{
    [SerializeField]
    Player_Manager.Player m_player;
    Text m_text;
    Score_Manager m_scoreManager;
    void Start()
    {
        m_text = GetComponent<Text>();
        m_scoreManager = FindObjectOfType<Score_Manager>();
    }
    // Update is called once per frame
    void Update()
    {
        m_text.text = m_scoreManager.GetScore(m_player).ToString();
    }
}
