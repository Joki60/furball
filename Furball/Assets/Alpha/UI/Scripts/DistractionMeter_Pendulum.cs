﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
[System.Serializable]
public class BarArea
{
    [SerializeField]
    float hideThreshold;
    [SerializeField]
    int weight;
    [SerializeField]
    Color color;
    [SerializeField]
    HIT hit;
    public int Weight{get{return weight;}}
    public float HideThreshold{get{return hideThreshold;}}
    public Color Color{get{return color;}}
    public HIT Hit{get{return hit;}}

    public enum HIT
    {
        Bad,
        Good,
        Perfect,
    }


    public Image left;
  
    public Image right;
}
public struct Area
{
    public float innerX;
    public float innerY;
    public float outerX;
    public float outerY;
}
public class DistractionMeter_Pendulum : MonoBehaviour
{
    /*[SerializeField]
    RectTransform m_activeCanvas;*/
    [SerializeField]
    Vector3 m_barWidth;
    [SerializeField]
    [Range(0,1)]
    float m_pendulum;
    [SerializeField]
    [Range(0,1)]
    float m_distractionAmount;
    [SerializeField]
    [Tooltip("How long the pendulum takes to swing to one side")]
    float m_pendulumTime;
    float m_pendulumTimer;
    private enum Direction
    {
        Right,
        Left
    }
    Direction m_direction = Direction.Right;
    [SerializeField]
    EasingFunction.Ease m_pendulumEase;
    EasingFunction.Function m_pendulumFunction;
    [SerializeField]
    BarArea[] m_areas;
    void Start()
    {
        m_pendulumFunction = EasingFunction.GetEasingFunction(m_pendulumEase);
    }

    // Update is called once per frame
    void Update()
    {

        if(m_direction == Direction.Right)
        {
            m_pendulumTimer += Time.deltaTime;
            if(m_pendulumTimer > m_pendulumTime)
            {
                m_direction = Direction.Left;
            }
        }
        else if(m_direction == Direction.Left)
        {
            m_pendulumTimer -= Time.deltaTime;
            if(m_pendulumTimer < 0)
            {
                m_direction = Direction.Right;
            }
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            print(GetHit(m_pendulum));
        }
        m_pendulumTimer = (m_pendulumTimer > 1)? 1:(m_pendulumTimer < 0)? 0 : m_pendulumTimer;
        m_pendulum = m_pendulumFunction(0,1,m_pendulumTimer/m_pendulumTime);
        GameObject.Find("Pendulum").transform.position = GetPendulumPosition();
        Display();
    }
    private void OnDrawGizmosSelected() 
    {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position,m_barWidth);
        BarArea[] p_areas = GetLegibleBarAreas();
        Area[] areas = GetLegibleAreas();

        
        for (int i = 0; i < areas.Length; i++)
        {
            Gizmos.color = p_areas[i].Color;
            Vector2 centerLEFT = GetWorldPosition(areas[i].innerX, areas[i].outerX);
            Vector2 centerRIGHT = GetWorldPosition(areas[i].innerY, areas[i].outerY);
            Vector2 size = GetSize(areas[i].innerX, areas[i].outerX);
            Gizmos.DrawCube(centerLEFT,size);
            Gizmos.DrawCube(centerRIGHT,size);
        }
    }      
    private Vector3 GetPendulumPosition()
    {
        return new Vector3(transform.position.x - m_barWidth.x/2 + ((m_barWidth.x * 2) -m_barWidth.x)* m_pendulum,GameObject.Find("Pendulum").transform.position.y);
    }
    public BarArea.HIT GetHit()
    {
        return GetHit(m_pendulum);
    }
    private BarArea.HIT GetHit(float p_value)
    {
        //There are some issues with accuracy
        if(p_value > 1 || p_value < 0)
        {
            print("Invalid value");
            return BarArea.HIT.Bad;
        }
        Area[] areas = GetLegibleAreas();
        BarArea[] barAreas = GetLegibleBarAreas();
        for(int i = 0; i < areas.Length; i++)
        {
            if(IsBetween(areas[i].outerX,areas[i].innerX,p_value) || IsBetween(areas[i].innerY,areas[i].outerY,p_value))
            {
                  return barAreas[i].Hit;
            }
        }
        return BarArea.HIT.Bad;
    }
    private Area[] GetLegibleAreas()
    {
        BarArea[] p_areas = GetLegibleBarAreas();
        int totalWeight = 0;
        foreach(BarArea item in p_areas)
        {
            totalWeight += item.Weight;
        }
        Area[] areas = new Area[p_areas.Length];
        for (int i = 0; i < p_areas.Length; i++)
        {
            if(i == 0)
            {
                areas[i].innerX = 0.5f;
                areas[i].innerY = 0.5f;
                areas[i].outerX = 0.5f - (m_distractionAmount*((float)p_areas[i].Weight/(float)totalWeight)/2);
                areas[i].outerY = 0.5f + (m_distractionAmount*((float)p_areas[i].Weight/(float)totalWeight)/2);
            }
            else
            {
                areas[i].innerX = areas[i-1].outerX;
                areas[i].innerY = areas[i-1].outerY;
                areas[i].outerX = areas[i].innerX - (m_distractionAmount*((float)p_areas[i].Weight/(float)totalWeight)/2);
                areas[i].outerY = areas[i].innerY + (m_distractionAmount*((float)p_areas[i].Weight/(float)totalWeight)/2);
            }
        }
        return areas;
    }
    private BarArea[] GetLegibleBarAreas()
    {
        List<BarArea> p_areas = new List<BarArea>();
        foreach (BarArea item in m_areas)
        {
            if(item.HideThreshold < m_distractionAmount){p_areas.Add(item);}
        }
        return p_areas.ToArray();
    }
    public Vector2 GetWorldPosition(float inner, float outer)
    {
      float delta = (inner + outer) /2;
      Vector2 center = new Vector2(transform.position.x - (m_barWidth.x/2) + (m_barWidth.x* delta),transform.position.y);
      return center;
    }
    public Vector2 GetSize(float inner, float outer)
    {
        return new Vector2(((inner - outer)/2 * (m_barWidth.x*2)), m_barWidth.y);
    }
    private bool IsBetween(float a, float b,float value)
    {
        return (value > a && value < b);
    }
    public void ModifyDistraction(float p_amount)
    {
        m_distractionAmount = (m_distractionAmount + p_amount < 0)? 0:(m_distractionAmount + p_amount > 1)? 1: m_distractionAmount + p_amount;
    }
    public void SetDistraction(float p_amount)
    {
        m_distractionAmount = (p_amount < 0)? 0: (p_amount > 1)? 1: p_amount;
    
    }
    public void Display()
    {
        BarArea[] t_barareas = GetLegibleBarAreas();
        Area[] t_areas = GetLegibleAreas();
        for (int i = 0; i < t_barareas.Length; i++)
        {
            t_barareas[i].left.rectTransform.localScale = new Vector3((t_areas[i].outerY - 0.5f)*2,t_barareas[i].left.rectTransform.localScale.y); 
            t_barareas[i].right.rectTransform.localScale = new Vector3((t_areas[i].outerY - 0.5f)*2,t_barareas[i].left.rectTransform.localScale.y);   
        }
    }
}
