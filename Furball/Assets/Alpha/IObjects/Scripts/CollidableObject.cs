﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class CollidableObject : MonoBehaviour
{
    public enum Directions
    {   
        SIDE,
        ABOVE,
        UNDER
    }
    [SerializeField]
    Directions[] m_directions;
    public Directions[] m_Directions{get{return m_directions;}}
    Cat m_cat;
    Player_Manager m_Player_Manger;
    bool m_hasCollided = false;
    void Start()
    {
        m_cat = GameObject.FindObjectOfType<Cat>();
        m_Player_Manger = GameObject.FindObjectOfType<Player_Manager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    virtual public void OnCollisionStay2D(Collision2D other)
    {
        OnCollision(other);
    }
    void OnCollision(Collision2D other)
    {
      if(!m_hasCollided && other.collider.tag == "Cat" && m_cat.IsColliding(this.gameObject))
        {
            print("IM COLLIDING");
            m_hasCollided = true;
            m_Player_Manger.UpdateActive(Player_Manager.Player.NONE);
        }
    }
}
