﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator),typeof(Collider2D))]
public class InteractibleObject : MonoBehaviour
{
    SoundManager m_SoundManger;
    [SerializeField]
    AudioClip m_onActiveAudio;

    //----Things for the distraction meter /mvh Elly
    [SerializeField]
    [Range(0,1)]
    float m_distractionAmount;
    bool m_Activated= false;


    void Awake()
    {
        if(m_onActiveAudio != null)
        {
            m_SoundManger = GameObject.Find("Game_Managers").transform.Find("Sound_Manager").GetComponent<SoundManager>();
            print(m_SoundManger);
        }
    }
    public void Activate()
    {
        if(m_onActiveAudio != null)
        {
            m_SoundManger.PlaySingle(m_onActiveAudio);
        }

        this.GetComponent<Animator>().SetTrigger("isActive");

        //---- More things for the distraction meter /mvh Elly
//        GameObject.FindGameObjectWithTag("Pendulum").GetComponent<DistractionMeter_Pendulum>().ModifyDistraction(m_distractionAmount);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Cat" && !m_Activated)
        {
            m_Activated = true;
            Activate();
        }
    }
}
