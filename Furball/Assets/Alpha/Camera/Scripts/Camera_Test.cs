﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Test : MonoBehaviour
{
    public float interpolationTime = 3;
    float interpolationTimer = 0;
    public EasingFunction.Ease ease;
    EasingFunction.Function funct;
    bool Switch = false;
    
    void Start()
    {
        funct = EasingFunction.GetEasingFunction(ease);
    }

    // Update is called once per frame
    void Update()
    {
        if(!Switch)
        interpolationTimer += Time.deltaTime;
        else if(Switch)
        interpolationTimer -= Time.deltaTime;
        if(Switch && interpolationTimer < 0)
        {
            Switch = false;
        }
        else if(!Switch && interpolationTimer > interpolationTime)
        {
            Switch = true;
        }
        
        transform.position = new Vector3(funct(-8,8,interpolationTimer/interpolationTime),0,-10);

        
    }
}

