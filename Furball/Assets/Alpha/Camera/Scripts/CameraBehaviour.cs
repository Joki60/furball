﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    [SerializeField]
    private Transform m_target;
    [SerializeField]
    private float m_smoothSpeed;
    [SerializeField]
    Vector3 m_offset;
    private Vector3 m_refVelocity = Vector3.zero;

    void LateUpdate()
    {
        Vector3 m_targetPosition = m_target.position + m_offset;
        transform.position = Vector3.SmoothDamp(transform.position, m_targetPosition, ref m_refVelocity, m_smoothSpeed, Mathf.Infinity,Time.deltaTime);
        
    }
    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z) {
        Ray ray = this.gameObject.GetComponent<Camera>().ScreenPointToRay(screenPosition);
        Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
}
